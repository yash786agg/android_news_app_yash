package com.app.newsapp.utils

class ConstantTest {

    companion object {

        const val TEST_TITLE_VLAUE = "Thousands line capital's streets for royal wedding"

        const val TEST_SUB_HEADLINE_VLAUE = "Well-wishers lined the Royal Mile in Edinburgh, where the Queen's granddaughter Zara Phillips married England rugby player Mike Tindall."
    }
}