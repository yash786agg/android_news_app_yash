package com.app.newsapp.ui.article.list

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.app.newsapp.R
import com.app.newsapp.utils.ConstantTest.Companion.TEST_SUB_HEADLINE_VLAUE
import com.app.newsapp.utils.ConstantTest.Companion.TEST_TITLE_VLAUE
import com.app.newsapp.utils.RecyclerViewMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class ArticleListActivityTest {

    @get:Rule
    var activityRule: ActivityTestRule<ArticleListActivity>
            = ActivityTestRule(ArticleListActivity::class.java,true,true)

    @Test
    fun matchDataInRecyclerView() {

        Thread.sleep(5000)
        onView(RecyclerViewMatcher(R.id.recylv_article)
                .atPositionOnView(2, R.id.title_tv))
                .check(matches(ViewMatchers.withText(TEST_TITLE_VLAUE)))


        onView(RecyclerViewMatcher(R.id.recylv_article)
                .atPositionOnView(2, R.id.subHeadLine_tv))
                .check(matches(ViewMatchers.withText(TEST_SUB_HEADLINE_VLAUE)))
    }
}