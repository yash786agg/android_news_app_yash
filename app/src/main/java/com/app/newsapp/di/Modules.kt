package com.app.newsapp.di

import com.app.newsapp.BuildConfig
import com.app.newsapp.commons.utils.UiHelper
import com.app.newsapp.datasource.api.NewsApi
import com.app.newsapp.datasource.network.createNetworkClient
import com.app.newsapp.domain.repository.ArticleRepository
import com.app.newsapp.domain.usecase.ArticleUseCase
import com.app.newsapp.ui.article.list.ArticleListVM
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import retrofit2.Retrofit

fun injectFeature() = loadFeature

private val loadFeature by lazy {
    loadKoinModules(
        listOf(viewModelModule,
            repositoryModule,
            networkModule,
            useCaseModule,
            uiHelperModule)
    )
}

val viewModelModule = module {
    viewModel { ArticleListVM(articleUseCase = get()) }
}

val repositoryModule = module {
    single { ArticleRepository(newsApi = get()) }
}

val useCaseModule = module {
    single { ArticleUseCase(articleRepository = get()) }
}

val networkModule = module {
    single { newsApi }
}

val uiHelperModule = module {
    single { UiHelper(androidContext()) }
}

private val retrofit : Retrofit = createNetworkClient(BuildConfig.BASE_URL)

private val newsApi : NewsApi = retrofit.create(NewsApi::class.java)