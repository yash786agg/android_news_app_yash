package com.app.newsapp.domain.entities

import android.os.Parcelable
import com.app.newsapp.commons.utils.Constants.Companion.ARTICLE_IMAGE_URL
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageData(@field:Json(name = ARTICLE_IMAGE_URL) val imageUrl: String) : Parcelable