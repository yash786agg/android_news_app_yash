package com.app.newsapp.domain.repository

import com.app.newsapp.commons.utils.Constants.Companion.COUNT_VALUE
import com.app.newsapp.commons.utils.Constants.Companion.FULL_VALUE
import com.app.newsapp.commons.utils.Constants.Companion.HEIGHT_VALUE
import com.app.newsapp.commons.utils.Constants.Companion.NAVIGATION_LEVEL_ID_VALUE
import com.app.newsapp.commons.utils.Constants.Companion.ORDER_BY_VALUE
import com.app.newsapp.commons.utils.Constants.Companion.WIDTH_VALUE
import com.app.newsapp.datasource.api.NewsApi

class ArticleRepository(private val newsApi: NewsApi) {

    suspend fun fetchArticle() = newsApi.getArticleAsync(COUNT_VALUE,NAVIGATION_LEVEL_ID_VALUE,
        ORDER_BY_VALUE,FULL_VALUE).await()

    suspend fun fetchImageData(photoId : String) = newsApi.getImageUrlAsync(photoId,WIDTH_VALUE,HEIGHT_VALUE).await()
}