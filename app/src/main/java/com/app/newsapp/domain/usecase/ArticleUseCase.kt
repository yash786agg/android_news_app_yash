package com.app.newsapp.domain.usecase

import androidx.lifecycle.MutableLiveData
import com.app.newsapp.datasource.api.NetworkState
import com.app.newsapp.domain.entities.ArticleData
import com.app.newsapp.domain.repository.ArticleRepository

class ArticleUseCase(private val articleRepository: ArticleRepository) {

    // FOR DATA ---
    private val networkState = MutableLiveData<NetworkState<Int>>()

    suspend fun execute(): List<ArticleData> {

        var articles : List<ArticleData> = listOf()

        networkState.postValue(NetworkState.Loading())

        val response = articleRepository.fetchArticle()

        if(response.isSuccessful) {
            response.body()?.map { fetchItemData(it) }

            val items = response.body()
            if(items?.size!! >= 0) articles = items

            networkState.postValue(NetworkState.Success(response.code()))
        } else networkState.postValue(NetworkState.Error(response.code()))

        return articles
    }

    private suspend fun fetchItemData(item: ArticleData) {
        val response = articleRepository.fetchImageData(item.id)
        if (response.isSuccessful) { item.imageUrl = response.body()?.imageUrl.toString() }
    }

    fun getNetworkState(): MutableLiveData<NetworkState<Int>> = networkState
}