package com.app.newsapp.domain.entities

import android.os.Parcelable
import com.app.newsapp.commons.utils.Constants.Companion.ARTICLE_IMAGE_URL
import com.app.newsapp.commons.utils.Constants.Companion.CONTENT_HTML_API_TAG
import com.app.newsapp.commons.utils.Constants.Companion.ID_API_TAG
import com.app.newsapp.commons.utils.Constants.Companion.PUBLISH_DATE_API_TAG
import com.app.newsapp.commons.utils.Constants.Companion.SUB_HEADLINE_API_TAG
import com.app.newsapp.commons.utils.Constants.Companion.TITLE_API_TAG
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ArticleData(
    @field:Json(name = ID_API_TAG) val id: String,
    @field:Json(name = TITLE_API_TAG) val title : String,
    @field:Json(name = PUBLISH_DATE_API_TAG) val publishDate: String,
    @field:Json(name = SUB_HEADLINE_API_TAG) val subHeadline: String,
    @field:Json(name = CONTENT_HTML_API_TAG) val htmlContent: String,
    @field:Json(name = ARTICLE_IMAGE_URL) var imageUrl: String
) : Parcelable