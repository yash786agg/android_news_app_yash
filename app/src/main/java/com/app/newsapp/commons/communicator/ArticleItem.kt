package com.app.newsapp.commons.communicator

import com.app.newsapp.domain.entities.ArticleData

interface ArticleItem {
    fun onArticleItemClickListener(articleData: ArticleData)
}