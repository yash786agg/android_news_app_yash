package com.app.newsapp.commons.utils

class Constants {

    companion object {

        const val TIMEOUT_REQUEST : Long = 30

        const val ARTICLE_URL = "articles/"

        const val IMAGE_URL = "images/"

        const val RENDITION = "/rendition/"

        const val ID_URL_TAG = "{id}"

        const val ID_TAG = "id"

        const val EXTRA_ARTICLE = "article"

        /* Article API Tag Value */

        const val COUNT_TAG : String = "count"

        const val NAVIGATION_LEVEL_ID_TAG : String = "navigationLevelId"

        const val ORDER_BY_TAG : String = "orderBy"

        const val FULL_TAG : String = "full"

        const val COUNT_VALUE : Int = 20

        const val NAVIGATION_LEVEL_ID_VALUE : Long = 1218

        const val ORDER_BY_VALUE : String = "ranking+ASC%252C+createdAt+ASC"

        const val FULL_VALUE : Int = 1

        /* Image API Tag Value */

        const val WIDTH_TAG : String = "width"

        const val WIDTH_VALUE : Int = 640

        const val HEIGHT_TAG : String = "height"

        const val HEIGHT_VALUE : Int = 360

        /* Article API Response Tag */

        const val ID_API_TAG : String = "id"

        const val TITLE_API_TAG : String = "title"

        const val PUBLISH_DATE_API_TAG : String = "publishDate"

        const val SUB_HEADLINE_API_TAG : String = "subHeadline"

        const val CONTENT_HTML_API_TAG : String = "contentHTML"

        const val ARTICLE_IMAGE_URL : String = "url"
    }
}