package com.app.newsapp.commons.utils

import android.content.Context
import android.net.ConnectivityManager
import android.view.View
import com.google.android.material.snackbar.Snackbar
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class UiHelper(private val context : Context) {

    fun getConnectivityStatus() : Boolean
    {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun showSnackBar(view: View, content: String) = Snackbar.make(view, content, Snackbar.LENGTH_LONG).show()

    /**
     * Return the publish Date Time after converting into a Readable Format
     * @param [publishDate] takes as a Input variable
     * @return the time in String format
     */

    fun getPublishedDateTime(publishDate : String) : String {

        var formattedTime = ""
        val initialDateFormat = "yyyy-MM-dd HH:mm:ss"
        val resultantDateFormat = "dd MMM yyyy, hh:mm a"
        val formatFrom = SimpleDateFormat(initialDateFormat, Locale.getDefault())
        formatFrom.isLenient = false
        val formatTo = SimpleDateFormat(resultantDateFormat, Locale.getDefault())
        formatTo.isLenient = false
        val date: Date?
        try
        {
            date = formatFrom.parse(publishDate)
            formattedTime = formatTo.format(date!!)
        }
        catch (e: ParseException)
        {
            e.printStackTrace()
        }

        return formattedTime
    }
}