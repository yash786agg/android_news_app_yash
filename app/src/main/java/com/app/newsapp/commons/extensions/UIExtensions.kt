package com.app.newsapp.commons.extensions

import android.os.Build
import android.text.Html
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import coil.api.load
import coil.request.CachePolicy
import com.app.newsapp.R
import com.app.newsapp.commons.utils.UiHelper

@BindingAdapter(value = ["articleItemImage"])
fun articleItemImage(view: ImageView, imageUrl: String?){

    if(!TextUtils.isEmpty(imageUrl)) {
        view.load(imageUrl) {
            crossfade(true)
            placeholder(R.drawable.place_holder)
            error(R.drawable.place_holder)
            memoryCachePolicy(CachePolicy.DISABLED)
        }
    }
}

@BindingAdapter(value = ["imageArticle"])
fun imageArticle(view: ImageView, imageUrl: String?){

    if(!TextUtils.isEmpty(imageUrl))
        view.load(imageUrl) {
            crossfade(true)
            placeholder(R.drawable.place_holder)
            error(R.drawable.place_holder)
            memoryCachePolicy(CachePolicy.DISABLED)
        }
}

@BindingAdapter("loadHtmlContent")
fun loadHtmlContent(textView: TextView, htmlTxtParams: String?) {
    if (!TextUtils.isEmpty(htmlTxtParams)){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            textView.text = Html.fromHtml(htmlTxtParams, Html.FROM_HTML_MODE_COMPACT)
        else textView.text = Html.fromHtml(htmlTxtParams)
    }
}

@BindingAdapter(value = ["publishDate"])
fun publishDate(textView : TextView, publishDate : String?) {
    if(!TextUtils.isEmpty(publishDate)) {

        val uiHelper = UiHelper(textView.context)
        val formattedTime = publishDate?.let { uiHelper.getPublishedDateTime(it) }

        if(!TextUtils.isEmpty(formattedTime) && formattedTime != "") textView.text = formattedTime
        else textView.visibility = View.GONE
    }
}