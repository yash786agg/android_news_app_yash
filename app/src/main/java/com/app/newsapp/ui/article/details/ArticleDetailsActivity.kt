package com.app.newsapp.ui.article.details

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.app.newsapp.R
import com.app.newsapp.commons.utils.Constants.Companion.EXTRA_ARTICLE
import com.app.newsapp.databinding.ActivityArticleDetailsBinding

class ArticleDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityArticleDetailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_article_details)

        if(intent != null && intent.hasExtra(EXTRA_ARTICLE))
            binding.article = intent.getParcelableExtra(EXTRA_ARTICLE)
    }
}