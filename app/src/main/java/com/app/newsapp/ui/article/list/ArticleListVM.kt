package com.app.newsapp.ui.article.list

import androidx.lifecycle.MutableLiveData
import com.app.newsapp.commons.base.BaseViewModel
import com.app.newsapp.datasource.api.NetworkState
import com.app.newsapp.domain.entities.ArticleData
import com.app.newsapp.domain.usecase.ArticleUseCase
import kotlinx.coroutines.launch

class ArticleListVM(private val articleUseCase: ArticleUseCase) : BaseViewModel() {

    // OBSERVABLES ---
    var networkState : MutableLiveData<NetworkState<Int>> = MutableLiveData()
    val articles : MutableLiveData<List<ArticleData>> = MutableLiveData()

    // UTILS ---
    init {
        handleCategoryLoad()
    }

    private fun handleCategoryLoad() {
        ioScope.launch { updateArticleLiveData(articleUseCase.execute()) }
        networkState = articleUseCase.getNetworkState()
    }

    private fun updateArticleLiveData(result: List<ArticleData>)
            = articles.postValue(result)
}