package com.app.newsapp.ui.article.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.newsapp.R
import com.app.newsapp.commons.communicator.ArticleItem
import com.app.newsapp.commons.extensions.DiffUtilCallBack
import com.app.newsapp.databinding.AdapterArticleListBinding
import com.app.newsapp.domain.entities.ArticleData
import kotlin.properties.Delegates

class ArticleListAdapter : RecyclerView.Adapter<ArticleListAdapter.MyViewHolder>(),
    DiffUtilCallBack {

    private var onArticleItemClickListener : ArticleItem? = null

    var items : List<ArticleData> by Delegates.observable(emptyList()) { _, oldItem, newItem ->
        autoNotify(oldItem, newItem) { old, new -> old.id == new.id }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: AdapterArticleListBinding = DataBindingUtil
            .inflate(LayoutInflater.from(parent.context), R.layout.adapter_article_list, parent, false)

        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position],onArticleItemClickListener)
    }

    // Gets the number of Items in the list
    override fun getItemCount(): Int = items.size

    fun setonArticleItemClickListener(onArticleItemItemClickListener : ArticleItem) {
        this.onArticleItemClickListener = onArticleItemItemClickListener
    }

    inner class MyViewHolder(private val binding: AdapterArticleListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(articleData: ArticleData, clickListener: ArticleItem?) {
            binding.article = articleData
            itemView.setOnClickListener { clickListener?.onArticleItemClickListener(articleData) }
        }
    }
}
