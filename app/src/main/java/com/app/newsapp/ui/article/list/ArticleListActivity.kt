package com.app.newsapp.ui.article.list

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.newsapp.R
import com.app.newsapp.commons.communicator.ArticleItem
import com.app.newsapp.commons.utils.Constants.Companion.EXTRA_ARTICLE
import com.app.newsapp.commons.utils.UiHelper
import com.app.newsapp.datasource.api.NetworkState
import com.app.newsapp.domain.entities.ArticleData
import com.app.newsapp.ui.article.details.ArticleDetailsActivity
import com.app.newsapp.ui.article.list.adapter.ArticleListAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class ArticleListActivity : AppCompatActivity(),ArticleItem {

    // FOR DATA ---
    private val articleVM : ArticleListVM by viewModel()
    private val uiHelper : UiHelper by inject()
    private val articleListAdapter = ArticleListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecyclerView()

        /*
         * Check Internet Connection
         * */

        if(uiHelper.getConnectivityStatus()) configureObservables()
        else uiHelper.showSnackBar(main_rootView, resources.getString(R.string.error_message_network))
    }

    private fun configureObservables() {

        articleVM.articles.observe(this, Observer {
            it?.let { articleListAdapter.items = it }
        })

        /*
         * Progress Updater
         * */
        articleVM.networkState.observe(this, Observer {
            if (it != null) {
                when (it) {
                    is NetworkState.Loading -> showProgressBar(true)
                    is NetworkState.Success ->  showProgressBar(false)
                    is NetworkState.Error -> {
                        showProgressBar(false)
                    }
                }
            }
        })
    }

    private fun initRecyclerView() {
        /*
         * Setup the adapter class for the RecyclerView
         * */
        recylv_article.layoutManager = LinearLayoutManager(this)
        recylv_article.adapter = articleListAdapter
        articleListAdapter.setonArticleItemClickListener(this)
    }

    // UPDATE UI ----
    private fun showProgressBar(display : Boolean) {
        if(!display) progress_bar.visibility = View.GONE
        else progress_bar.visibility = View.VISIBLE
    }

    override fun onArticleItemClickListener(articleData: ArticleData) {

        articleData.let {
            val intent = Intent(this, ArticleDetailsActivity::class.java)
            intent.putExtra(EXTRA_ARTICLE,articleData)
            startActivity(intent)
        }
    }

}
