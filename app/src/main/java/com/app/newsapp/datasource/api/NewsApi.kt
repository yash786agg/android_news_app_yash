package com.app.newsapp.datasource.api

import com.app.newsapp.BuildConfig
import com.app.newsapp.commons.utils.Constants.Companion.ARTICLE_URL
import com.app.newsapp.commons.utils.Constants.Companion.COUNT_TAG
import com.app.newsapp.commons.utils.Constants.Companion.FULL_TAG
import com.app.newsapp.commons.utils.Constants.Companion.HEIGHT_TAG
import com.app.newsapp.commons.utils.Constants.Companion.ID_TAG
import com.app.newsapp.commons.utils.Constants.Companion.ID_URL_TAG
import com.app.newsapp.commons.utils.Constants.Companion.IMAGE_URL
import com.app.newsapp.commons.utils.Constants.Companion.NAVIGATION_LEVEL_ID_TAG
import com.app.newsapp.commons.utils.Constants.Companion.ORDER_BY_TAG
import com.app.newsapp.commons.utils.Constants.Companion.RENDITION
import com.app.newsapp.commons.utils.Constants.Companion.WIDTH_TAG
import com.app.newsapp.domain.entities.ArticleData
import com.app.newsapp.domain.entities.ImageData
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NewsApi {

    @GET(BuildConfig.BASE_URL+ARTICLE_URL)
    fun getArticleAsync(@Query(COUNT_TAG) count: Int,
                        @Query(NAVIGATION_LEVEL_ID_TAG) navigationLevelId: Long,
                        @Query(ORDER_BY_TAG) orderBy : String,
                        @Query(FULL_TAG) full : Int) : Deferred<Response<List<ArticleData>>>

    @GET(BuildConfig.BASE_URL+IMAGE_URL+ID_URL_TAG+RENDITION)
    fun getImageUrlAsync(@Path(ID_TAG) id: String,
                         @Query(WIDTH_TAG) width: Int,
                         @Query(HEIGHT_TAG) height: Int) : Deferred<Response<ImageData>>
}