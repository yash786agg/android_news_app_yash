package com.app.newsapp.commons.util

class ConstantTest {

    companion object {

        const val ARTICLE_ID : String = "504"
        
        const val ARTICLE_TITLE : String = "Violent prisoner who failed to return to open jail arrested in Edinburgh"

        const val ARTICLE_PUBLISH_DATE : String = "2011-09-21 09:11:00"

        const val ARTICLE_SUB_HEADLINE : String = "'Abscond'"

        const val ARTICLE_HTML_CONTENT : String = "<p><strong>A violent prisoner who failed to return to an open jail has been arrested by police."

        const val ARTICLE_IMAGE_URL : String = "http://api.stv.tv/images/504/rendition/?width=640&height=640"

        const val EXPECTED_SUCCESS_RESPONSE_CODE : Int = 200

        const val EXPECTED_ERROR_RESPONSE_CODE : Int = 404
    }
}