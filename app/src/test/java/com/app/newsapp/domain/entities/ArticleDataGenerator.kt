package com.app.newsapp.domain.entities

object ArticleDataGenerator {
    fun getSuccessArticleData() : ArticleData {

        return ArticleData(
            "504",
            "Violent prisoner who failed to return to open jail arrested in Edinburgh",
            "2011-09-21 09:11:00",
            "'Abscond' prisoner arrested",
            "<p><strong>A violent prisoner who failed to return to an open jail has been arrested by police.<\\/strong><\\/p><p>Samuel Stewart failed to return to HMP Castle Huntly, near Dundee, last Wednesday after a period of home leave near Edinburgh.<\\/p><p>He was traced by police and arrested in Edinburgh on Wednesday morning after being missing for a week.<\\/p><p>The 47-year-old, who is originally from Edinburgh, will be reported to the procurator fiscal before appearing from custody at Perth Sheriff Court on Thursday.<\\/p><p>Stewart was serving more than five years in jail after being convicted of assault to severe injury and permanent disfigurement at the High Court in Edinburgh in August 2009.<\\/p><p>He was transferred from Saughton Prison in Edinburgh to Castle Huntly in January this year.<\\/p>\\n\",\n" +
                    "\"contentMarkdown\":\"**A violent prisoner who failed to return to an open jail has been arrested by police.**\\n\\nSamuel Stewart failed to return to HMP Castle Huntly, near Dundee, last Wednesday after a period of home leave near Edinburgh.\\n\\nHe was traced by police and arrested in Edinburgh on Wednesday morning after being missing for a week.\\n\\nThe 47-year-old, who is originally from Edinburgh, will be reported to the procurator fiscal before appearing from custody at Perth Sheriff Court on Thursday.\\n\\nStewart was serving more than five years in jail after being convicted of assault to severe injury and permanent disfigurement at the High Court in Edinburgh in August 2009.\\n\\nHe was transferred from Saughton Prison in Edinburgh to Castle Huntly in January this year.",
            "http://api.stv.tv/images/504/rendition/?width=640&height=640")
    }

    fun getEmptyArticleData() : ArticleData {
        return ArticleData("", "","","","","")
    }

    fun getSuccessImageData() : ImageData {
        return ImageData("http://api.stv.tv/images/504/rendition/?width=640&height=640")
    }

    fun getEmptyImageData() : ImageData {
        return ImageData("")
    }
}