package com.app.newsapp.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.app.newsapp.commons.util.ConstantTest.Companion.EXPECTED_ERROR_RESPONSE_CODE
import com.app.newsapp.commons.util.ConstantTest.Companion.EXPECTED_SUCCESS_RESPONSE_CODE
import com.app.newsapp.datasource.api.NetworkState
import com.app.newsapp.domain.entities.ArticleDataGenerator
import com.app.newsapp.domain.repository.ArticleRepository
import org.junit.Rule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.junit.Test
import retrofit2.Response
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.junit.Assert.assertEquals
import org.mockito.ArgumentMatchers.anyString

@RunWith(JUnit4::class)
class ArticleUseCaseTest {

    @Rule
    @JvmField
    val instantTaskExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    private val articleRepository: ArticleRepository = mock()
    private val articleUseCase = ArticleUseCase(articleRepository)

    @Test
    fun verifyResultWhenRepoMockReturnSuccessState() {

        runBlocking {

            val result = Response.success(listOf(ArticleDataGenerator.getSuccessArticleData()))
            given(articleRepository.fetchArticle())
                .willReturn(result)

            val response = Response.success(ArticleDataGenerator.getSuccessImageData())

            given(articleRepository.fetchImageData(anyString()))
                .willReturn(response)

            val realResult = articleUseCase.execute()
            val expectedResult = Response.success(listOf(ArticleDataGenerator.getSuccessArticleData()))

            assertEquals(expectedResult.body(), realResult)
        }
    }

    @Test
    fun verifyUseCaseCallRepository() {

        val result = Response.success(listOf(ArticleDataGenerator.getEmptyArticleData()))

        runBlocking {
            given(articleRepository.fetchArticle())
                .willReturn(result)

            val response = Response.success(ArticleDataGenerator.getEmptyImageData())

            given(articleRepository.fetchImageData(anyString()))
                .willReturn(response)

            articleUseCase.execute()

            verify(articleRepository, times(1)).fetchArticle()
            verify(articleRepository, times(1)).fetchImageData(anyString())
        }
    }

    @Test
    fun verifyIsLoadingLiveDataWhenResultIsSuccess() {
        val expectedResult = 200
        articleUseCase.getNetworkState().value = NetworkState.Success(expectedResult)
        assertEquals(
            (articleUseCase.getNetworkState().value as NetworkState.Success<Int>).code,
            EXPECTED_SUCCESS_RESPONSE_CODE
        )
    }

    @Test
    fun verifyIsLoadingLiveDataWhenResultIsError() {
        val expectedResult = 404
        articleUseCase.getNetworkState().value = NetworkState.Error(expectedResult)
        assertEquals(
            (articleUseCase.getNetworkState().value as NetworkState.Error<Int>).code,
            EXPECTED_ERROR_RESPONSE_CODE
        )
    }
}