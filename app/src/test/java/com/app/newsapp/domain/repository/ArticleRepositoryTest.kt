package com.app.newsapp.domain.repository

import com.app.catalog.commons.util.SequenceList
import com.app.newsapp.commons.util.ConstantTest.Companion.ARTICLE_HTML_CONTENT
import com.app.newsapp.commons.util.ConstantTest.Companion.ARTICLE_ID
import com.app.newsapp.commons.util.ConstantTest.Companion.ARTICLE_IMAGE_URL
import com.app.newsapp.commons.util.ConstantTest.Companion.ARTICLE_PUBLISH_DATE
import com.app.newsapp.commons.util.ConstantTest.Companion.ARTICLE_SUB_HEADLINE
import com.app.newsapp.commons.util.ConstantTest.Companion.ARTICLE_TITLE
import com.app.newsapp.domain.entities.ArticleDataGenerator
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import retrofit2.Response

@RunWith(JUnit4::class)
class ArticleRepositoryTest {

    private val articleRepository: ArticleRepository = mock()

    @Test
    fun verifyApiModelToEntityModelMustReturnSameValues() {

        val response = Response.success(ArticleDataGenerator.getSuccessArticleData())

        runBlocking {
            Mockito.`when`(articleRepository.fetchArticle())
                .thenAnswer(SequenceList(listOf(response)))
        }

        val expectedResult = Response.success(ArticleDataGenerator.getSuccessArticleData())

        assertEquals(expectedResult.body(), response.body())

        assertEquals(ARTICLE_ID, response.body()?.id)
        assertTrue(response.body()?.title == ARTICLE_TITLE)
        assertTrue(response.body()?.imageUrl == ARTICLE_IMAGE_URL)
        assertTrue(response.body()?.publishDate == ARTICLE_PUBLISH_DATE)
        assertNotNull("", response.body()?.imageUrl)
        assertFalse(response.body()?.subHeadline == ARTICLE_SUB_HEADLINE)
        assertFalse(response.body()?.htmlContent == ARTICLE_HTML_CONTENT)
    }

    @Test
    fun verifyResultWhenRepoMockReturnEmptyState() {

        val response = Response.success(ArticleDataGenerator.getEmptyArticleData())

        runBlocking {
            Mockito.`when`(articleRepository.fetchArticle()).thenAnswer(SequenceList(listOf(response)))
        }

        val expectedResult = Response.success(ArticleDataGenerator.getEmptyArticleData())

        assertEquals(expectedResult.body(), response.body())

        assertEquals("", response.body()?.id)
        assertEquals("", response.body()?.title)
        assertFalse(response.body()?.publishDate == ARTICLE_PUBLISH_DATE)
        assertFalse(response.body()?.imageUrl == ARTICLE_IMAGE_URL)
    }
}