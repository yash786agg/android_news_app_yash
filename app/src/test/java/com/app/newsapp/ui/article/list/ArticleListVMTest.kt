package com.app.newsapp.ui.article.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.app.newsapp.domain.entities.ArticleData
import com.app.newsapp.domain.entities.ArticleDataGenerator
import com.app.newsapp.domain.usecase.ArticleUseCase
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

@RunWith(JUnit4::class)
class ArticleListVMTest {

    @Rule
    @JvmField
    val instantTaskExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    private var articleUseCase : ArticleUseCase = mock()
    private lateinit var viewModel: ArticleListVM

    @Rule
    @JvmField
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Test
    fun verifyArticleLiveData() {
        runBlocking {
            givenSuccessResult()
            whenViewModelHandleLoadArticle()
            assertEquals(null, viewModel.articles.value?.get(0)?.id)
        }
    }

    private fun givenSuccessResult() {
        runBlocking {
            val result: List<ArticleData> = listOf(ArticleDataGenerator.getEmptyArticleData())
            given(articleUseCase.execute()).willReturn(result)
        }
    }

    private fun whenViewModelHandleLoadArticle() {
        viewModel = ArticleListVM(articleUseCase)
    }
}