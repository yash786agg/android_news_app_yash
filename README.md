# android_news_app_yash

The application contains a list of the articles with images of a Article. Data is fetched
dynamically from a web server.


# Demo
![android_news_app_yash](screenshots/news_app_demo.gif)

## Languages, libraries and tools used

* __[Kotlin](https://developer.android.com/kotlin)__
* __[Kotlin Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html)__
* __[Koin](https://github.com/InsertKoinIO/koin)__
* __[Coil](https://coil-kt.github.io/coil/getting_started/)__
* __[Android Material Design](https://material.io/components/)__
* __[Android Architecture Components](https://developer.android.com/topic/libraries/architecture/index.html)__
* __[Instrumented Unit Testing](https://developer.android.com/training/testing/unit-testing/instrumented-unit-tests)__
* __[Espresso Testing](http://developer.android.com/training/testing/espresso)__

Above Features are used to make code simple, generic, understandable, clean and easily maintainable
for future development. Especially **Koin** for dependency injection and **Kotlin Coroutines** for
asynchronous API call.

This application supports the screen rotation without losing the data and also use **Constraintlayout** to design layout which
gives better **UI support for both Mobile and Tablet**, and even when the screen rotates.

As this app developed as a production-ready app. So I had enabled the **Proguard** along with minifyEnabled
and shrinkResources True in released version along with basic support for **Android App Bundle**.
In the result of that, we can reduce the APK size along with we can also provide an extra security layer
to our code from decompiling or extracting of the code from APK. For more info refer the Image below.

![Apk Analyser](screenshots/News_App_Apk_Analyser.png)

## Running and Building the application

You can run the app on a real device or an emulator.

* __[Run on a real device](https://developer.android.com/training/basics/firstapp/running-app#RealDevice)__
* __[Run on an emulator](https://developer.android.com/training/basics/firstapp/running-app#Emulator)__


# Prerequisites
* __Android Studio 3.5.3__
* __Gradle version 3.5.3__
* __Kotlin version 1.3.61__
* __Android Device with USB Debugging Enabled__

# Built With

* __[Android Studio](https://developer.android.com/studio/index.html)__ - The Official IDE for Android
* __[Kotlin](https://developer.android.com/kotlin)__ - Language used to build the application
* __[Gradle](https://gradle.org)__ - Build tool for Android Studio